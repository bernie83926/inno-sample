//
//  ParkInfoCell.swift
//  TaipeiPark
//
//  Created by 蔡育庭 on 2018/2/5.
//  Copyright © 2018年 蔡育庭. All rights reserved.
//

import UIKit

class ParkInfoCell: UITableViewCell {
    
    @IBOutlet weak var title:UILabel!
    @IBOutlet weak var subtitle:UILabel!
    @IBOutlet weak var note:UILabel!
    @IBOutlet weak var imgView:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
