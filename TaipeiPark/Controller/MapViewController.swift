//
//  SecondViewController.swift
//  TaipeiPark
//
//  Created by 蔡育庭 on 2018/2/5.
//  Copyright © 2018年 蔡育庭. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapVC: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate{

    @IBOutlet weak var MapView: MKMapView!
    @IBOutlet weak var numLabel: UILabel!
    @IBAction func sLide(sender: UISlider){
        
        numLabel.text = "\(Int(sender.value))"
        MapView.removeAnnotations(MapView.annotations)
        makeAnnotation(limit: Int(sender.value))

    }
    
    let locationManger = CLLocationManager()
    var annotation = MKPointAnnotation()
    var CafeData = [CafeInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        locationManger.delegate = self
        locationManger.distanceFilter = kCLLocationAccuracyNearestTenMeters;
        locationManger.desiredAccuracy = kCLLocationAccuracyBest;
        
        MapView.delegate = self
        MapView.showsUserLocation = true
        MapView.userTrackingMode = .follow
        
        MapView.showsScale = true
        
        WebClass.instance.getCoffeeData(handler: { (response) in
            
            self.CafeData = response
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                self.sortDistance()
            }
            
            self.makeAnnotation(limit: 30)
            
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManger.requestWhenInUseAuthorization()
            locationManger.startUpdatingLocation()
        }else if CLLocationManager.authorizationStatus() == .denied { // Denied!
            let alertController = UIAlertController(
                title: "定位權限已關閉",
                message:"如要變更權限，請至 設定 > 隱私權 > 定位服務 開啟",
                preferredStyle: .alert)
            let okAction = UIAlertAction(title: "確認", style: .default, handler:nil)
            alertController.addAction(okAction)
            self.present(alertController,animated: true, completion: nil)
        }else if CLLocationManager.authorizationStatus() == .authorizedWhenInUse { // Accept!
            locationManger.startUpdatingLocation()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //-------------------------------------------------------------
    
    func makeAnnotation(limit: Int) {
        
        for Data in self.CafeData {
            
            if self.MapView.annotations.count >= limit + 1 { // include user's location
                break
            }
            
            self.annotation = MKPointAnnotation()
            self.annotation.coordinate = CLLocation(latitude: Data.latitude, longitude: Data.longitude).coordinate
            self.annotation.title = Data.name
            self.annotation.subtitle = Data.address
            self.MapView.addAnnotation(self.annotation)
            
        }
    }
    
    func sortDistance()  {  // sort location from nearest to farthest
        for i in 0..<CafeData.count {
            let point = CLLocation(latitude: self.CafeData[i].latitude, longitude: self.CafeData[i].longitude)
            self.CafeData[i].distance = Double((locationManger.location?.distance(from: point))!)
        }
        self.CafeData.sort()
    }

}

extension CafeInfo: Comparable {  // sort extension

    static func <(lhs: CafeInfo, rhs: CafeInfo) -> Bool {
        return lhs.distance! < rhs.distance!
    }

    static func ==(lhs: CafeInfo, rhs: CafeInfo) -> Bool {
        return  lhs.distance == rhs.distance
    }

}

