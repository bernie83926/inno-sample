//
//  FirstViewController.swift
//  TaipeiPark
//
//  Created by 蔡育庭 on 2018/2/5.
//  Copyright © 2018年 蔡育庭. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class ParkListTVC: UITableViewController {

    var data:[ParkInfo]? = nil
    var searchData:[ParkInfo]? = nil
    
    var loadView:UIView? = nil
    var searchBar:UISearchBar?
    let mask = UIView()
    
    var rowNumber = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupSearchBar()
        setupInfiniteScrollingView()
        
        let barItem = UIBarButtonItem(customView: self.searchBar!)
        
        self.tableView.tableFooterView = self.loadView
        self.navigationItem.leftBarButtonItem = barItem
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.red], for: .normal)
        // change color of cancelButton
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        WebClass.instance.getParkData(handler: { (response) in
            self.data = response
            self.rowNumber += 20
            self.tableView.reloadData()
        }) //Load Data
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------------------------------
    
    func setupSearchBar() {  // init searchBar
        
        let searchBar: UISearchBar = UISearchBar(frame: CGRect(x:0, y:0, width:self.view.bounds.width, height:28))
        searchBar.delegate = self
        searchBar.placeholder = "搜尋"
        searchBar.tintColor = UIColor.white
        searchBar.sizeToFit()
        searchBar.backgroundColor = UIColor.clear
        searchBar.autocapitalizationType = UITextAutocapitalizationType.none
        searchBar.keyboardType = UIKeyboardType.default
        
        self.searchBar = searchBar
        
    }
    
    
    
    func setupInfiniteScrollingView() {  // init buttom loading view
        
        self.loadView = UIView(frame: CGRect(x:0, y:self.tableView.contentSize.height,
                                                 width:self.tableView.bounds.size.width, height:60))
        self.loadView!.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.loadView!.backgroundColor = UIColor.clear
        
        let activityViewIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activityViewIndicator.color = UIColor.darkGray
        let indicatorX = self.loadView!.frame.size.width / 2 - activityViewIndicator.frame.width / 2    //center
        let indicatorY = self.loadView!.frame.size.height / 2 - activityViewIndicator.frame.height / 2  //center
        
        activityViewIndicator.frame = CGRect(x: indicatorX, y: indicatorY, width: activityViewIndicator.frame.width,
                                             height: activityViewIndicator.frame.height)
        activityViewIndicator.startAnimating()
        
        self.loadView!.addSubview(activityViewIndicator)
        
    }
    
    func maskOn() {  // create an invisible mask touched will dismiss keyboard
        self.mask.frame = CGRect(x:0, y:0, width:self.view.bounds.width, height:self.view.bounds.height)
        self.mask.backgroundColor = UIColor.clear
        self.mask.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ParkListTVC.maskOff)))
        self.tableView.addSubview(mask)
        self.tableView.isScrollEnabled = false
    }

    @objc func maskOff() {
        self.searchBar?.resignFirstResponder()
        self.mask.removeFromSuperview()
        self.tableView.isScrollEnabled = true
    }
    
}

extension ParkListTVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return searchData != nil ? (searchData?.count)! : self.rowNumber
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ParkInfo", for: indexPath) as! ParkInfoCell
        if data != nil {
            let info = searchData != nil ? searchData![indexPath.row] : data![indexPath.row]
            cell.tag = indexPath.row
            cell.title.text = info.parkname
            cell.subtitle.text = info.name
            cell.note.text = info.description != "" ? info.description : "暫無描述"  // why not nil??
            cell.imgView.kf.setImage(with: URL(string: info.imgUrl), placeholder: UIImage(named: "items_default"))
//            if let img = ShareData.instance.imageCache[info.imgUrl] {
//                cell.imgView.image = img
//            }else{
//                cell.imgView.image = nil
//                WebClass.instance.loadImage(url: info.imgUrl, handler: { (Image) in
//                    if cell.tag == indexPath.row {   // make sure imageView will load correct image
//                        cell.imgView.image = Image
//                    }
//                })
//            }  //Cache!
               //There might be a better way...
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == tableView.numberOfRows(inSection: 0) - 1 {  //buttom
            rowNumber = rowNumber + 20 > data!.count ? data!.count : rowNumber + 20
            let count = searchData != nil ? (searchData?.count)! : self.rowNumber
            if tableView.numberOfRows(inSection: 0) == count {
                tableView.tableFooterView = nil    // remove loadview if all data have been loaded
            }else{
                if tableView.tableFooterView == nil {
                    tableView.tableFooterView = self.loadView
                }
                
                tableView.reloadData()
            }
            
        }
    }
    
}

extension ParkListTVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        maskOn()
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        maskOff()
        self.searchData = data?.filter({ (data) -> Bool in
            return data.parkname.contains(find: searchBar.text!) == true ? true : data.name.contains(find: searchBar.text!)
            // sort name and parkname in data
            })
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        searchData = nil
        maskOff()
        self.tableView.reloadData()
    }
    
}

