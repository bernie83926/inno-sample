//
//  CafeInfo.swift
//  TaipeiPark
//
//  Created by 蔡育庭 on 2018/2/7.
//  Copyright © 2018年 蔡育庭. All rights reserved.
//

import Foundation

class CafeInfo {
    
    private var _latitude:Double
    private var _longitude:Double
    private var _name:String
    private var _address:String
    public var distance:Double? = nil
    
    var name:String {
        return _name
    }
    
    var address:String {
        return _address
    }
    
    var latitude:Double {
        return _latitude
    }
    
    var longitude:Double {
        return _longitude
    }
    
    init(latitude:Double, longitude:Double, name:String, address:String) {
        self._name = name
        self._address = address
        self._latitude = latitude
        self._longitude = longitude
    }
    
}


