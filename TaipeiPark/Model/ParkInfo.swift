//
//  Model.swift
//  TaipeiPark
//
//  Created by 蔡育庭 on 2018/2/5.
//  Copyright © 2018年 蔡育庭. All rights reserved.
//

import Foundation

class ParkInfo {
    
    private var _parkname:String
    private var _name:String
    private var _time:String
    private var _imgUrl:String
    private var _description:String
    
    var parkname:String {
        return _parkname
    }
    
    var name:String{
        return _name
    }
    
    var time:String{
        return _time
    }
    
    var imgUrl:String{
        return _imgUrl
    }
    
    var description:String{
        return _description
    }
    
    
    init(parkname:String, name:String, time:String ,imgUrl:String ,description:String) {
        self._parkname = parkname
        self._name = name
        self._time = time
        self._imgUrl = imgUrl
        self._description = description
    }
}
