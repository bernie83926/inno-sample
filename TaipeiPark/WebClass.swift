//
//  WebClass.swift
//  TaipeiPark
//
//  Created by 蔡育庭 on 2018/2/5.
//  Copyright © 2018年 蔡育庭. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire


class WebClass {
    
    static let instance = WebClass()
    
    func getParkData(handler: @escaping (_ responses: [ParkInfo]) -> Void ){
        let url = "http://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=bf073841-c734-49bf-a97f-3757a6013812"
                
        Alamofire.request(url, method: .get, parameters: [:])  // [limit:20] to limit how many data to get
            .responseJSON { (response) in
                
                response.result.ifSuccess {
                    
                    let dataRes = JSON(response.result.value)["result"]["results"]
                    var dataArray = [ParkInfo]()
                    
                    for i in 0..<dataRes.count {
                        let info = ParkInfo.init(parkname: dataRes[i]["ParkName"].string!, name: dataRes[i]["Name"].string!, time: dataRes[i]["OpenTime"].string!, imgUrl: dataRes[i]["Image"].string!, description: dataRes[i]["Introduction"].string!)
                        dataArray.append(info)
                    }
                    
                    handler(dataArray)
                }
                
                response.result.ifFailure {
                    print("failed to get data")
                }
        }
    }
    
    func getCoffeeData(handler: @escaping (_ responses: [CafeInfo]) -> Void) {
        let url = "https://cafenomad.tw/api/v1.2/cafes/taipei"
        
        Alamofire.request(url, method: .get, parameters: [:])
            .responseJSON { (response) in
                
                response.result.ifSuccess {
                    let dataRes = JSON(response.result.value)
                    var dataArray = [CafeInfo]()
                    
                    for i in 0..<dataRes.count {
                        
                        let info = CafeInfo.init(latitude: Double(dataRes[i]["latitude"].string!)!, longitude: Double(dataRes[i]["longitude"].string!)!, name: dataRes[i]["name"].string!, address: dataRes[i]["address"].string!)
                        dataArray.append(info)
                    }
                    
                    handler(dataArray)
                }
                response.result.ifFailure {
                    print("failed to get data")
                }
        }
    }
    
    func loadImage(url:String, handler: @escaping (_ img: UIImage?) -> Void){
        let request = URLRequest(url: URL(string: url)!)
        NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) {
            (response: URLResponse!, data: Data!, error: Error!) -> Void in
            handler(UIImage(data: data))
            ShareData.instance.imageCache[url] = UIImage(data: data)  //Cache
        }
    }

}
